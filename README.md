# VK Service app

## Description

* App was created to launch all VK services from one place.
* If the services are installed on your device you can open it from VKSeervices app or open web page with these services

## Screenshots
<img src="screenshots/screenshot.png" width="400" height="790">

## Techonologies

* UI Components - `UIKit`
* Architecture - `Model-Viwe-Controller`
* 100% Programmatical UI
* 100% Programmatical `AutoLayout` 
* Was used `UINavigationController`
* Was used `UITabelView`
* Was created custom `UITableViewCells`
* Asynchronous fetch JSON data and Image data from url 
* App takes data from JSON and shows it by UITabelView
* User can open VK service app or service web-page by tap on each cell
* Was added some comments to methods
* Were added Unit tests and UI tests    

## Getting Started

### Dependencies

* IPhone or IPad  
* IOS 15

### Installing

* Download from GitLab

### Executing program

* Run it with `XCode`

## Authors

* Roman Khomenko  
* Telegram - [@romanKhomenko]
* VK - (https://vk.com/tolstiykroll)

## Version History

* 0.1
    * Initial Release

