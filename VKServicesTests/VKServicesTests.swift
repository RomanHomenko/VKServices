//
//  VKServicesTests.swift
//  VKServicesTests
//
//  Created by Роман Хоменко on 15.07.2022.
//

import XCTest
@testable import VKServices

class VKServicesTests: XCTestCase {
    
    var networkManager: NetworkManager!
    var viewController: ViewController!

    override func setUpWithError() throws {
        networkManager = NetworkManager.shared
        viewController = ViewController()
    }
    
    override func tearDownWithError() throws {
        networkManager = nil
        viewController = nil
    }
    
    func testSuccesFetchJSONData() {
        // Given
        let url = jsonURL
        var vkServicesAPI: VKServiceAPI? = nil
        
        // When
        networkManager.fetchServicesData(from: url) { servicesData in
            vkServicesAPI = servicesData
        }
        
        // Then
        XCTAssertNil(vkServicesAPI, "Error to fetch data with URL: \(jsonURL)")
    }
}
