//
//  VKServicesUITests.swift
//  VKServicesUITests
//
//  Created by Роман Хоменко on 15.07.2022.
//

import XCTest

class VKServicesUITests: XCTestCase {
    
    func testCellTap() {
        let app = XCUIApplication()
        app.launch()
        
        app.tables.cells.element(boundBy: 0).tap()
    }
}
