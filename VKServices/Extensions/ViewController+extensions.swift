//
//  VieweController+extensions.swift
//  VKServices
//
//  Created by Роман Хоменко on 15.07.2022.
//

import Foundation
import UIKit

// MARK: - Setup NavigationController
extension ViewController {
    func setupNavController() {
        navigationItem.title = navTitle
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.titleTextAttributes = navTextAttributes
    }
}

// MARK: - Setup TableView
extension ViewController {
    func setupTableView() {
        servicesTableView.dataSource = self
        servicesTableView.delegate = self
        servicesTableView.translatesAutoresizingMaskIntoConstraints = false
        
        servicesTableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        servicesTableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        servicesTableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor).isActive = true
        servicesTableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
    }
}

// MARK: - Add new views to VC
extension ViewController {
    func addViewsToScreen() {
        addNewSubview(newView: servicesTableView)
    }
    
    func addNewSubview(newView: UIView) {
        view.addSubview(newView)
    }
}
