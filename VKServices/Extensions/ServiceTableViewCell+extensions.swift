//
//  ServiceTableViewCell+extensions.swift
//  VKServices
//
//  Created by Роман Хоменко on 15.07.2022.
//

import Foundation
import UIKit

// MARK: - Create, add subviews to view and add Constraints
extension ServiceTableViewCell {
    func addConstraints() {
        // MARK: - Add views to contentView
        self.contentView.addSubview(iconImageView)
        containerView.addSubview(titleLabel)
        containerView.addSubview(descriptionLabel)
        self.contentView.addSubview(containerView)
        
        // MARK: - Add constraints
        // Image view
        iconImageView.centerYAnchor.constraint(equalTo: self.contentView.centerYAnchor).isActive = true
        iconImageView.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: offsetConst).isActive = true
        iconImageView.widthAnchor.constraint(equalToConstant: imageWidthConst).isActive = true
        iconImageView.heightAnchor.constraint(equalToConstant: imageHeightConst).isActive = true
        
        // Container view
        containerView.centerYAnchor.constraint(equalTo: self.contentView.centerYAnchor).isActive = true
        containerView.leadingAnchor.constraint(equalTo: iconImageView.trailingAnchor, constant: offsetConst).isActive = true
        containerView.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -offsetConst).isActive = true
        containerView.heightAnchor.constraint(equalToConstant: containerHeightConst).isActive = true
        
        // Title label
        titleLabel.topAnchor.constraint(equalTo: containerView.topAnchor).isActive = true
        titleLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: containerView.trailingAnchor).isActive = true
        
        // Description label
        descriptionLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor).isActive = true
        descriptionLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor).isActive = true
        descriptionLabel.trailingAnchor.constraint(equalTo: containerView.trailingAnchor).isActive = true
        descriptionLabel.bottomAnchor.constraint(equalTo: containerView.bottomAnchor).isActive = true
    }
}
