//
//  ServiceTableViewCell.swift
//  VKServices
//
//  Created by Роман Хоменко on 15.07.2022.
//

import UIKit

class ServiceTableViewCell: UITableViewCell {
    // MARK: - Create Service Model
    var service: Service? {
        didSet {
            guard let serviceItem = service else { return }
            guard let image = try? Data(contentsOf: URL(string: serviceItem.iconURL)!) else { return }
            iconImageView.image = UIImage(data: image)
            titleLabel.text = serviceItem.name
            descriptionLabel.text = serviceItem.serviceDescription
        }
    }
    
    // MARK: - Create views
    let iconImageView: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFill
        image.translatesAutoresizingMaskIntoConstraints = false
        image.layer.cornerRadius = cellCornerRadius
        image.clipsToBounds = true
        
        return image
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: cellTitleFontSize)
        label.textColor = whiteTextColor
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    let descriptionLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: cellDescriptionsFontSize)
        label.numberOfLines = descriptionLines
        label.textColor = whiteTextColor
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    let containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.clipsToBounds = true
        
        return view
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        addConstraints()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    
}
