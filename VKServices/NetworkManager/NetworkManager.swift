//
//  NetworkManager.swift
//  VKServices
//
//  Created by Роман Хоменко on 15.07.2022.
//

import Foundation

class NetworkManager {
    static let shared = NetworkManager()
    
    func fetchServicesData(from url: String, completion: @escaping(VKServiceAPI) -> Void) {
        guard let url = URL(string: jsonURL) else { return }
        
        var request = URLRequest(url: url, timeoutInterval: Double.infinity)
        request.httpMethod = httpGetMethod
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            DispatchQueue.main.async {
                guard let data = data else {
                    return
                }
                do {
                    let vkServices = try JSONDecoder().decode(VKServiceAPI.self, from: data)
                    completion(vkServices)
                } catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
        
        task.resume()
    }
}
