//
//  ViewController.swift
//  VKServices
//
//  Created by Роман Хоменко on 15.07.2022.
//

import UIKit

class ViewController: UIViewController {
    // MARK: - Properties
    var vkServices: [Service] = []
    
    // MARK: - Creating tableView
    let servicesTableView: UITableView = {
        let table = UITableView()
        table.showsVerticalScrollIndicator = false
        table.showsVerticalScrollIndicator = false
        
        // register tableViewCell
        table.register(ServiceTableViewCell.self,
                       forCellReuseIdentifier: cellID)
        
        return table
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addViewsToScreen()
        setupNavController()
        setupTableView()
        
        getServicesData()
    }
}

// MARK: - Get services from API
extension ViewController {
    // Take data from NetworkManager by singltone
    func getServicesData() {
        NetworkManager.shared.fetchServicesData(from: jsonURL) { [weak self] services in
            self?.vkServices = services.body.services
            self?.servicesTableView.reloadData()
        }
    }
}

// MARK: - Conform to TableViewDataSource and TableViewDelegate
extension ViewController: UITableViewDelegate, UITableViewDataSource {
    // number of row depends on vkServices array
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vkServices.count
    }
    
    // Settings cell with json data
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID,
                                                       for: indexPath) as! ServiceTableViewCell
        cell.service = vkServices[indexPath.row]
        cell.accessoryType = .disclosureIndicator
        
        return cell
    }
    
    // set Height of tbaleVieewCell
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let application = UIApplication.shared
        let websiteURL = URL(string: vkServices[indexPath.row].link)!
        
        // will successfully launch an app that can handle the URL
        if application.canOpenURL(websiteURL) {
            let secondAppURL = websiteURL
            application.open(secondAppURL)
        } else {
            application.open(websiteURL)
        }
    }
}
