//
//  Constants.swift
//  VKServices
//
//  Created by Роман Хоменко on 15.07.2022.
//
import UIKit

// cell
public let cellID = "service"
public let cellHeight: CGFloat = 80
public let cellCornerRadius: CGFloat = 5
public let cellTitleFontSize: CGFloat = 20
public let cellDescriptionsFontSize: CGFloat = 12
public let offsetConst: CGFloat = 10
public let imageWidthConst: CGFloat = 60
public let imageHeightConst: CGFloat = 60
public let containerHeightConst: CGFloat = 60
public let descriptionLines = 2

// nav bar
public let navTitle = "Сервисы VK"
public let navTextAttributes = [NSMutableAttributedString.Key.foregroundColor: whiteTextColor]

// text
public let whiteTextColor: UIColor = .white

// Networking
public let httpGetMethod = "GET"
public let jsonURL = "https://publicstorage.hb.bizmrg.com/sirius/result.json"
